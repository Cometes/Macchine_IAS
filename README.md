## Esercitazioni di "Macchine a fluido" LM IAS a.a. 2018/2019
 
Benvenuto nel repository del materiale didattico per il corso di **Macchine a fluido** per la laurea Magistrale in Ingegneria Aerospaziale. 
Il repository è organizzato in cartelle ognuna delle quali contiene i testi degli esercizi svolti in aula. 

**Recapiti**

+ ufficio: Via Venezia 1, DII-V, piano 5, "Cometes":
+ mail to: francesco.devanna@phd.unipd.it

